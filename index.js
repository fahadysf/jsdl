#!/usr/bin/env node
import chalk from 'chalk';
import clear from 'clear';
import figlet from 'figlet';
import request from 'request';

// Local includes
import './lib/files.cjs'
import files from './lib/files.cjs';
import path from 'path';


clear();
console.log(
    chalk.green(
        figlet.textSync('JS Downloader', { horizontalLayout: 'full' })
    )
);
console.log('Curent full path is: ' + path.resolve('./'));
console.log('Current dir is: ' + files.getCurrentDirectoryBase())