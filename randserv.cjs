const http = require('http')
const fs = require('fs')
const Mustache = require('mustache');
const dayjs = require('dayjs');
const RequestIp = require('@supercharge/request-ip')

const myArgs = process.argv.slice(2);
const timeFormat = 'DD-MM-YYYY HH:mm:ss (UTCZZ)';
const template = fs.readFileSync('./templates/index.mustache', 'utf-8');
const port = process.env.PORT || 8030


function serve_random(req, res) {
    const client_ip = RequestIp.getClientIp(req);
    const timestamp = dayjs();
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    if (!req.url.endsWith('favicon.ico'))
        console.log(`[${timestamp.format(timeFormat)}] Request from ${client_ip} - URL: ${req.url}`);
    var data = {
        random_data: '',
        ip_address: client_ip,
        user_agent: req.headers['user-agent'],
        timestamp: timestamp.format(timeFormat),
        full_headers: JSON.stringify(req.headers, null, 2)
    };
    for (var i = 0; i < 20; i = i + 1)
        data.random_data = data.random_data + generateRandomString(128) + '\n';
    var output = Mustache.render(template, data);
    res.end(output);
}

const generateRandomString = (myLength) => {
    const chars =
        "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890";
    const randomArray = Array.from(
        { length: myLength },
        (v, k) => chars[Math.floor(Math.random() * chars.length)]
    );
    const randomString = randomArray.join("");
    return randomString;
};


var server = http.createServer(serve_random)
server.listen(port, () => {
    console.log(`Server running at port ${port}`);
})