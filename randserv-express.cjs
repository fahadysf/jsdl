const express = require('express');
const fs = require('fs')
const Mustache = require('mustache');
const dayjs = require('dayjs');
const RequestIp = require('@supercharge/request-ip')
const crypto = require('node:crypto')

const timeFormat = 'DD-MM-YYYY HH:mm:ss (UTCZZ)';
const template = fs.readFileSync('./templates/index.mustache', 'utf-8');
const port = process.env.PORT || 8031

var app = express();
app.use(express.json());

const generateRandomString = (myLength) => {
    const chars =
        "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890";
    const randomArray = Array.from(
        { length: myLength },
        (v, k) => chars[Math.floor(Math.random() * chars.length)]
    );
    const randomString = randomArray.join("");
    return randomString;
};

function serve_random(req, res) {
    const client_ip = RequestIp.getClientIp(req);
    const timestamp = dayjs();
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    if (!req.url.endsWith('favicon.ico'))
        console.log(`[${timestamp.format(timeFormat)}] Request from ${client_ip} - URL: ${req.url}`);
    var data = {
        random_data: '',
        ip_address: client_ip,
        user_agent: req.headers['user-agent'],
        timestamp: timestamp.format(timeFormat),
        full_headers: JSON.stringify(req.headers, null, 2)
    };
    for (var i = 0; i < 20; i = i + 1)
        data.random_data = data.random_data + generateRandomString(128) + '\n';
    var output = Mustache.render(template, data);
    res.end(output);
}

function calculate_data_hash(req, res) {
    res.setHeader('Content-Type', 'application/json');
    if (req.body.data) {
        const sha256hash = crypto.Hash('sha256');
        sha256hash.update(req.body.data, 'utf-8');
        res.statusCode = 200;
        res.json({
            data: req.body.data,
            sha256hash: sha256hash.digest('hex')
        })
    }
    else {
        res.statusCode = 503;
        res.json({ errror: 'No data provided in POST body', post: req.body });
    }
    res.end()
}

app.get('/', serve_random);
app.post('/calchash', calculate_data_hash);

var server = app.listen(port, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Random Server app listening at http://%s:%s', host, port);
});